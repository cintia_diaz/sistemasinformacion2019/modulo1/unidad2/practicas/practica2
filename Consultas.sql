﻿/* PRACTICA NUMERO: 2  */

 USE m1u2practica2;

/* CONSULTA 1 */
-- Indicar el número de ciudades que hay en la tabla ciudades
   SELECT 
    COUNT(*) 
   FROM ciudad c;


/* CONSULTA 2 */
-- Indicar el nombre de las ciudades que tengan una población por encima de la población media
   SELECT
    c.nombre 
   FROM ciudad c
    WHERE c.población>
    (
     SELECT 
       AVG(c1.población) 
     FROM ciudad c1);


/* CONSULTA 3 */
-- Indicar el nombre de las ciudades que tengan una población por debajo de la población media
    SELECT 
      c.nombre 
    FROM ciudad c
      WHERE c.población<
        (
         SELECT 
           AVG(c1.población) 
         FROM ciudad c1);
  


/* CONSULTA 4 */
-- Indicar el nombre de la ciudad con la población máxima

    -- c1
    -- Calculo la poblacion máxima
    SELECT 
      MAX(c.población) maximo
    FROM ciudad c;


    -- c2
    -- La ciudad con la poblacion maxima calculada
    SELECT c.nombre
      FROM ciudad c 
        WHERE c.población=(
          SELECT MAX(c.población) FROM ciudad c
        );
    
    -- con JOIN
      SELECT c.nombre FROM
        (SELECT 
           MAX(c.población) maximo
         FROM ciudad c) c1 
        JOIN ciudad c 
        ON c.población=c1.maximo;

/* CONSULTA 5 */
-- Indicar el nombre de la ciudad con la población mínima

    -- c1
    -- Calculo la poblacion mínima
    SELECT 
      MIN(c.población) minimo
    FROM ciudad c;

    -- c2
    -- La ciudad con la poblacion minima calculada
    SELECT c.nombre FROM ciudad c
      WHERE c.población=(
        SELECT 
          MIN(c.población) minimo
        FROM ciudad c
      );

    -- con JOIN
      SELECT c.nombre FROM 
        (
          SELECT 
            MIN(c.población) minimo
          FROM ciudad c
        ) c1
        JOIN 
        ciudad c
        ON c.población=c1.minimo;


/* CONSULTA 6 */
-- Indicar el número de ciudades que tengan una población por encima de la población media

  -- c1
  -- calculo la poblacion media
    SELECT AVG(c.población) FROM ciudad c;

  -- c2
  -- cuento cuantas ciudades tienen mas poblacion que la media
    SELECT COUNT(*) numero
      FROM ciudad c 
    WHERE c.población>(
      SELECT AVG(c.población) FROM ciudad c
      );

    
/* CONSULTA 7 */
-- Indicarme el número de personas que viven en cada ciudad

    SELECT ciudad, COUNT(*) numero
      FROM persona p
      GROUP BY p.ciudad;


/* CONSULTA 8 */
-- Utilizando la tabla trabaja indicarme cuantas personas trabajan en cada una de las compañias
  
    SELECT 
      t.compañia, COUNT(*) 
      FROM trabaja t
      GROUP BY t.compañia;


/* CONSULTA 9 */
-- Indicarme la compañía que mas trabajadores tiene
    
  -- c1
  -- contar numero de trabajadores por compañia
    SELECT 
      t.compañia, COUNT(*) ntrabajadores
      FROM trabaja t
      GROUP BY t.compañia;

  -- c2
  -- maximo del numero de trabajadores
    SELECT MAX(c1.ntrabajadores) FROM (
      SELECT t.compañia, COUNT(*) ntrabajadores
        FROM trabaja t
        GROUP BY t.compañia
    ) c1; 


    -- final
    SELECT c1.compañia FROM (
        SELECT 
          t.compañia, COUNT(*) ntrabajadores
        FROM trabaja t
        GROUP BY t.compañia
        ) c1
      JOIN (
        SELECT MAX(c1.ntrabajadores) maximo FROM (
          SELECT t.compañia, COUNT(*) ntrabajadores
            FROM trabaja t
            GROUP BY t.compañia
        )c1
      ) c2
      ON c1.ntrabajadores=c2.maximo;




/* CONSULTA 10 */
-- Indicarme el salario medio de cada una de las compañias
    SELECT 
      t.compañia, AVG(t.salario) salarioMedio
    FROM trabaja t 
    GROUP BY t.compañia;


/* CONSULTA 11 */
-- Listarme el nombre de las personas y la población de la ciudad donde viven
    SELECT 
      p.nombre, c.población 
    FROM persona p 
    JOIN ciudad c 
    ON p.ciudad=c.nombre;

/* CONSULTA 12 */
-- Listar el nombre de las personas, la calle donde vive y la población de la ciudad donde vive
   SELECT 
     p.nombre, p.calle, c.población 
   FROM persona p 
   JOIN ciudad c 
   ON p.ciudad=c.nombre;


/* CONSULTA 13 */
-- Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja
   SELECT 
      p.nombre npersona, p.ciudad vive, c.ciudad trabaja 
   FROM persona p 
   JOIN trabaja t 
   ON p.nombre=t.persona 
   JOIN compañia c 
   ON t.compañia=c.nombre;



/* CONSULTA 14 */
-- Realizar el algebra relacional y explicar la siguiente consulta

   /*
    Indica los nombres de las persona que viven y trabajan en la misma ciudad, ordenados por su nombre
   */

  SELECT persona.nombre
    FROM persona, trabaja, compañia 
    WHERE persona.nombre=trabaja.persona
    AND trabaja.compañia=compañia.nombre
    AND compañia.ciudad=persona.ciudad
    ORDER BY persona.nombre;



/* CONSULTA 15 */
-- Listarme el nombre de la persona y el nombre de su supervisor
   SELECT
    p.nombre, s.supervisor   
   FROM persona p 
   JOIN supervisa s 
   ON p.nombre=s.persona;

   
/* CONSULTA 16 */
-- Listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada una de ellos
  

  SELECT 
    s.supervisor nombreSupervisor, psupervisor.ciudad ciudadSupervisor , s.persona nombreSupervisa,p.ciudad ciudadSupervisa 
  FROM supervisa s 
  JOIN persona p 
  JOIN persona psupervisor
  ON s.persona=p.nombre AND s.supervisor=psupervisor.nombre;
  

/* CONSULTA 17 */
-- Indicarme el número de ciudades distintas que hay en la tabla compañía
  SELECT 
    COUNT(DISTINCT c.ciudad) 
  FROM compañia c;


/* CONSULTA 18 */
-- Indicarme el número de ciudades distintas que hay en la tabla personas
  SELECT 
    COUNT(DISTINCT p.ciudad) 
  FROM persona p; 


/* CONSULTA 19 */
-- Indicarme el nombre de las personas que trabajan para FAGOR
  SELECT 
    t.persona 
  FROM trabaja t 
  WHERE t.compañia='FAGOR';

  
/* CONSULTA 20 */
-- Indicarme el nombre de las personas que no trabajan para FAGOR
  SELECT
    t.persona 
  FROM trabaja t 
  WHERE t.compañia <>'FAGOR';


/* CONSULTA 21 */
-- Indicarme el número de personas que trabajan para INDRA
  SELECT
    COUNT(*) nPersonas
  FROM trabaja t 
  WHERE t.compañia='INDRA';

/* CONSULTA 22 */
-- Indicarme el nombre de las personas que trabajan para FAGOR o para INDRA
  SELECT 
    t.persona 
  FROM trabaja t 
  WHERE t.compañia='FAGOR'
    UNION
  SELECT 
    t.persona 
  FROM trabaja t 
  WHERE t.compañia='INDRA';


/* CONSULTA 23 */
-- Listar la población donde vive cada persona, sus salario, su nombre y la compañía para la que trabaja. Ordenar la
-- salida por nombre de la persona y por salario de forma descendente
  
  SELECT 
   c.población, t.salario, p.nombre, t.compañia
  FROM ciudad c 
  JOIN persona p 
  ON c.nombre=p.ciudad
  JOIN trabaja t
  ON p.nombre=t.persona
  ORDER BY p.nombre, t.salario DESC;