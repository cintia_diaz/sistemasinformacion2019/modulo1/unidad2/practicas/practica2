﻿/* creando las bases de datos */

DROP DATABASE IF EXISTS m1u2practica2;
CREATE DATABASE m1u2practica2;

USE m1u2practica2;

CREATE TABLE Ciudad (
  nombre Varchar(30) PRIMARY KEY,
  población Integer
);

CREATE TABLE Persona (
  nombre Varchar(30) PRIMARY KEY,
  calle Varchar(30),
  ciudad Varchar(30)
 );

CREATE TABLE Compañia (
  nombre Varchar(30) PRIMARY KEY,
  ciudad Varchar(30)
);

CREATE TABLE Trabaja (
  persona Varchar(30) PRIMARY KEY,
  compañia Varchar(30),
  salario Integer
);

CREATE TABLE Supervisa (
  supervisor Varchar(30),
  persona Varchar(30),
  PRIMARY KEY (supervisor,persona)
);

